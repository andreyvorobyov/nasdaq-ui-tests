package hrytsenko.nasdaq;

import hrytsenko.nasdaq.page.IndustryPage;

import java.util.Arrays;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

/**
 * Tests for {@link IndustryPage}.
 * 
 * @author hrytsenko.anton
 */
public class IndustryPageTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndustryPageTest.class);

    private static final int INDUSTRIES_NUM = 12;
    private static final int MAJOR_INDICES_NUM = 3;

    /**
     * Exceptions for names of industries.
     */
    private static final Map<String, String> NAMES = ImmutableMap.of("Healthcare", "Health Care", "Consumer Non-Dur.",
            "Consumer Non-Durables");

    private static String nameOf(String industry) {
        return NAMES.getOrDefault(industry, industry);
    }

    /**
     * Test filtering by industry.
     */
    @Test
    public void testFilter() {
        IndustryPage.open().ensure(p -> Assert.assertEquals("Companies by Industry", p.title()))
                .ensure(p -> Assert.assertTrue(p.activeFilters().isEmpty()))
                .ensure(p -> Assert.assertEquals(INDUSTRIES_NUM, p.industries().size())).industries().stream()
                .peek(i -> LOGGER.info("Filter by {}.", nameOf(i))).forEach(i -> filterByIndustry(i));
    }

    private void filterByIndustry(String industry) {
        IndustryPage.open(nameOf(industry))
                .ensure(p -> Assert.assertEquals(String.join(" ", nameOf(industry), "Companies"), p.title()))
                .ensure(p -> Assert.assertEquals(Arrays.asList(industry), p.activeFilters()));
    }

    /**
     * Test the pop-up ticker.
     */
    @Test
    public void testTicker() {
        IndustryPage.open().showTicker().ensure(p -> Assert.assertEquals(MAJOR_INDICES_NUM, p.majorIndices().size()))
                .hideTicker();
    }

}
